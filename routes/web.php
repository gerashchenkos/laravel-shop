<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::middleware(['check.role:admin'])->group(function () {
        Route::prefix('admin')->group(function () {
            Route::get('/', 'AdminController@index')->name('admin.home');
            Route::resource('users', 'AdminUserController');
            Route::resource('products', 'AdminProductController');
            Route::get('/export/product/{product}', 'AdminProductController@export')->name('admin.export');
            Route::get('/products-data', 'AdminProductController@productsData')->name('admin.products_data');
            /*Route::get('/test', function () {
                return view('welcome');
            });*/
        });
    });
    Route::middleware(['check.status', 'check.role:customer'])->group(function () {
        Route::prefix('product')->group(function () {
            Route::get('/', 'ProductController@index')->name('product.home');
            Route::post('/filtered', 'ProductController@filtered')->name('product.filtered');
            Route::get('/filtered', 'ProductController@index');
            Route::post('/search', 'ProductController@search');
        });

        Route::prefix('cart')->group(function () {
            Route::post('/store', 'CartController@store')->name('cart.store');
            Route::get('/', 'CartController@index')->name('cart.show');
            Route::post('/update', 'CartController@update')->name('cart.update');
            Route::get('/reset', 'CartController@reset')->name('cart.reset');
        });

        Route::prefix('order')->group(function () {
            Route::post('/store', 'OrderController@store')->name('order.store');
            Route::get('/create', 'OrderController@create')->name('order.create');
            Route::get('/thankyou/{order}', 'OrderController@thankyou')->name('order.thankyou');
        });
    });
});
