<?php

return [
    'default_product_image' => env(
        'DEFAULT_PRODUCT_IMAGE',
        'unnamed.png'
    ),
    'default_product_image_path' => env(
        'DEFAULT_PRODUCT_IMAGE_PATH',
        'public/products'
    ),
];
