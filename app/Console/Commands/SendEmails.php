<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendemail:test {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Simple email sending';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::raw('Hi, welcome ' . $this->argument('name').'!', function ($message) {
            $message->to("t@t.com")
            ->subject("Test!");
        });
    }
}
