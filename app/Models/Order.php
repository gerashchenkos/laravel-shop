<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';
    protected $fillable = ['user_id', 'cart_id', 'amount', 'charge_id', 'receipt_url'];

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

    public function cart()
    {
        return $this->hasOne('App\Models\Cart');
    }
}
