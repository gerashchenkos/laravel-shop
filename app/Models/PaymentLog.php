<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payment_log';
    protected $fillable = ['user_id', 'cart_id', 'status', 'error_message', 'api_response'];

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

    public function cart()
    {
        return $this->hasOne('App\Models\Cart');
    }
}
