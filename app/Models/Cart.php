<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cart';
    protected $fillable = ['user_id', 'total_price'];

    public function cart_product()
    {
        return $this->hasMany('App\Models\CartProduct', 'cart_id', 'id');
    }
}
