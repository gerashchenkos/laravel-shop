<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\PaymentLogRepository;
use Illuminate\Support\Collection;
use App\Models\PaymentLog;

class PaymentLogService extends BaseService
{
    public function __construct(PaymentLogRepository $repo)
    {
        $this->repo = $repo;
    }
}
