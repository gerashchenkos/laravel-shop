<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\CartRepository;
use App\Services\CartProductService;
use Illuminate\Support\Collection;
use App\Models\Cart;

class CartService extends BaseService
{
    public function __construct(CartRepository $repo, CartProductService $cartProduct)
    {
        $this->repo = $repo;
        $this->cartProduct = $cartProduct;
    }

    public function show(int $id): Cart
    {
        return $this->repo->show($id);
    }

    public function updateProducts(int $id, array $productQuantity)
    {
        foreach ($productQuantity as $key => $val) {
            $productId = explode("_", $key)[1];
            if ($val == 0) {
                $this->cartProduct->deleteProduct($id, $productId);
            } else {
                $this->cartProduct->updateProduct($id, $productId, ["quantity" => $val]);
            }
            $this->cartProduct->recalculateTotalPrice($id);
        }
        return true;
    }

}
