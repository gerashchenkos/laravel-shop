<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\CategoryRepository;
use Illuminate\Support\Collection;

class CategoryService extends BaseService
{
    public function __construct(CategoryRepository $repo)
    {
        $this->repo = $repo;
    }
}
