<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\ProductRepository;
use Illuminate\Support\Collection;
use App\Models\Product;

class ProductService extends BaseService
{
    public function __construct(ProductRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getByCategories(array $categories): Collection
    {
        return $this->repo->getByCategories($categories);
    }

    public function all(int $cartId = null): Collection
    {
        return $this->repo->all($cartId);
    }

    public function search(string $word): Collection
    {
        return $this->repo->search($word);
    }
}
