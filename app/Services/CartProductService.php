<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\CartProductRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CartProductService extends BaseService
{
    CONST POPULAR_LIMIT = 3;

    public function __construct(CartProductRepository $repo)
    {
        $this->repo = $repo;
    }

    public function addProducts(int $cartId, array $products)
    {
        foreach($products as $product) {
            $this->repo->addProduct($cartId, $product);
        }
        $this->recalculateTotalPrice($cartId);
    }

    public function recalculateTotalPrice(int $cartId): float
    {
        $totalPrice = $this->repo->recalculateTotalPrice($cartId);
        resolve('App\Services\CartService')->update($cartId, ['total_price' => $totalPrice]);
        return $totalPrice;
    }

    public function deleteProduct(int $cartId, int $productId): bool
    {
        return $this->repo->deleteProduct($cartId, $productId);
    }

    public function updateProduct(int $cartId, int $productId, array $data): bool
    {
        return $this->repo->updateProduct($cartId, $productId, $data);
    }

    public function getPopular(): array
    {
        /*if (Cache::store('database')->has('popular_products')) {
            return json_decode(Cache::store('database')->get('popular_products'), true);
        } else {
            $popularProducts = $this->repo->getPopular(self::POPULAR_LIMIT);
            Cache::store('database')->put('popular_products', json_encode($popularProducts), 60);
            return $popularProducts->toArray();
        }*/
        if (Cache::store('redis')->has('popular_products')) {
            return json_decode(Cache::store('redis')->get('popular_products'), true);
        } else {
            $popularProducts = $this->repo->getPopular(self::POPULAR_LIMIT);
            Cache::store('redis')->put('popular_products', json_encode($popularProducts), 60);
            return $popularProducts->toArray();
        }
    }
}
