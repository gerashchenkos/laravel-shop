<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\OrderRepository;
use Illuminate\Support\Collection;
use App\Models\Order;

class OrderService extends BaseService
{
    public function __construct(OrderRepository $repo)
    {
        $this->repo = $repo;
    }
}
