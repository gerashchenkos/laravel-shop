<?php

namespace App\Services;

use App\Services\BaseService;
use Illuminate\Support\Collection;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Auth;

class StripeService extends BaseService
{
    public function __construct()
    {
        $this->stripe = Stripe::make(env('STRIPE_SECRET_KEY'));
    }

    public function pay(array $data): array
    {
        try {
            $user = Auth()->user();
            if (empty($user->stripe_customer_id)) {
                $customer = $this->stripe->customers()->create(
                    [
                        'email' => $user->email,
                        'name' => $user->name
                    ]
                );
                $user->stripe_customer_id = $customer['id'];
                $user->save();
            }

            $token = $this->stripe->tokens()->create(
                [
                    'card' => [
                        'number' => $data['card_number'],
                        'exp_month' => $data['exp_month'],
                        'cvc' => $data['cvv'],
                        'exp_year' => $data['exp_year']
                    ],
                ]
            );

            $card = $this->stripe->cards()->create($user->stripe_customer_id, $token['id']);

            $customer = $this->stripe->customers()->update(
                $user->stripe_customer_id,
                [
                    'default_source' => $card['id'],
                ]
            );

            //charge
            $charge = $this->stripe->charges()->create(
                [
                    'customer' => $customer['id'],
                    'currency' => 'UAH',
                    'amount' => $data['amount'],
                ]
            );
        } catch (\Exception $e) {
            $message = $e->getMessage();
            return ["status" => "error", "error" => $message, "data" => $e];
        }
        return ["status" => "ok", "data" => $charge];
    }
}
