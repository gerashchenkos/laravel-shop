<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class OrderExist extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->route('id');
        return true;
    }

    public function all($keys = null) 
    {
       $data = parent::all($keys);
       $data['id'] = $this->route('order')->id;
       return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = Auth::user()->id;
        return [
            "id" => [
                'required',
                Rule::exists('orders')->where(function ($query) use ($userId) {
                    $query->where('user_id', $userId);
                }),
            ]
        ];
    }
}
