<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;
use App\Models\Product;
use App\Http\Requests\ProductStore;
use App\Http\Requests\ProductEdit;
use PDF;
use DataTables;
use Illuminate\Support\Facades\View;

class AdminProductController extends Controller
{
    public function __construct(ProductService $product, CategoryService $category)
    {
        $this->product = $product;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_product.index', ["products" => $this->product->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_product.create', ['categories' => $this->category->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStore $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStore $request)
    {
        $path = $request->file('image')->store(config('variables.default_product_image_path'));
        $imageName = str_replace(config('variables.default_product_image_path') . DIRECTORY_SEPARATOR, "", $path);
        $this->product->create($request->except('image') + ["image" => $imageName]);
        return redirect()->route("products.index");
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin_product.show', ["product" => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin_product.edit', ["product" => $product, 'categories' => $this->category->all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductEdit $request
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductEdit $request, Product $product)
    {
        $data = $request->all();
        unset($data['image']);
        if (!empty($request->file('image'))) {
            if (!empty($product->image)) {
                $path = $request->file('image')->storeAs(
                    config('variables.default_product_image_path'),
                    $product->image
                );
            } else {
                $path = $request->file('image')->store(config('variables.default_product_image_path'));
                $data['image'] = str_replace(config('variables.default_product_image_path') . DIRECTORY_SEPARATOR, "", $path);
            }
        }
        $this->product->update($product->id, $data);
        return redirect()->route('products.show', ['product' => $product->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function export(Product $product)
    {
        $pdf = PDF::loadView('pdf.product', ["product" => $product]);
        $pdf->save('product.pdf');
    }

    public function productsData()
    {
        $products = $this->product->all()->toArray();
        foreach($products as &$product){
            $product['category_name'] = $product['category']['name'];
            $product['action'] = View::make('admin_product.action_block', ['product' => $product])->render();
        } unset($product);
        return DataTables::of($products)->make(true);
    }
}
