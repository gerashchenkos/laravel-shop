<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;
use App\Services\CartProductService;
use App\Http\Requests\ProductsFiltered;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\View;

class ProductController extends Controller
{
    public function __construct(ProductService $products, CategoryService $categories, CartProductService $cartProduct)
    {
        $this->products = $products;
        $this->categories = $categories;
        $this->cartProduct = $cartProduct;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $popular = $this->cartProduct->getPopular();
        return view(
            'product.index',
            [
                "products" => $this->products->all(session('cart_id')),
                "categories" => $this->categories->all(),
                "disableDates" => ['07/12/2020'],
                "popularProducts" => $popular
            ]
        );
    }

    public function filtered(ProductsFiltered $request)
    {
        $products = $this->products->getByCategories($request->categories);
        return view(
            'product.index',
            [
                "products" => $products,
                "categories" => $this->categories->all(),
                "selected_categories" => $request->categories
            ]
        );
    }

    public function search(Request $request)
    {
        $products = $this->products->search($request->name);
        $productsHtml = View::make('product.products_block', ['products' => $products])->render();
        return response()->json(
            [
                'message' => 'OK',
                'products' => $productsHtml
            ]
        );
    }
}
