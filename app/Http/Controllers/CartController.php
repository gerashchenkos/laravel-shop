<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CartService;
use App\Http\Requests\CartStore;
use App\Http\Requests\CartUpdate;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function __construct(CartService $cart)
    {
        $this->cart = $cart;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (empty(session('cart_id'))) {
            return redirect()->route('product.home');
        }
        $cart = $this->cart->show(session('cart_id'));
        return view('cart.index', ["cart" => $cart]);
    }

    public function store(CartStore $request)
    {
        if (empty(session('cart_id'))) {
            $cart = $this->cart->create(["user_id" => Auth::id(), "total_price" => 0]);
            session(['cart_id' => $cart->id]);
        }
        $this->cart->cartProduct->addProducts(session('cart_id'), $request->only('products')['products']);
        return redirect()->route('cart.show');
    }

    public function update(CartUpdate $request)
    {
        $products = $request->except(['_token', 'update']);
        if ($this->cart->updateProducts(session('cart_id'), $products)) {
            return redirect()->route('cart.show');
        } else {
            //redirect with error
            return redirect()->route('cart.show');
        }
    }

    public function reset()
    {
        if (!empty(session('cart_id'))) {
            $this->cart->destroy(session('cart_id'));
            session()->forget('cart_id');
        }
        return redirect()->route('product.home');
    }
}
