<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\CartService;
use App\Http\Requests\CreditCard;
use App\Http\Requests\OrderExist;
use App\Services\StripeService;
use App\Services\OrderService;
use App\Services\PaymentLogService;
use App\Models\Order;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;

class OrderController extends Controller
{
    public function __construct(
        CartService $cart,
        StripeService $stripe,
        OrderService $order,
        PaymentLogService $paymentLog
    ) {
        $this->cart = $cart;
        $this->stripe = $stripe;
        $this->order = $order;
        $this->paymentLog = $paymentLog;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    }

    public function create()
    {
        if (empty(session('cart_id'))) {
            return redirect()->route('product.home');
        }
        $cart = $this->cart->show(session('cart_id'));
        return view('order.create', ["cart" => $cart]);
    }

    public function store(CreditCard $request)
    {
        $data = $request->all();
        $cart = $this->cart->show(session('cart_id'));
        $data['amount'] = $cart->total_price;
        $result = $this->stripe->pay($data);
        if ($result['status'] == "ok") {
            $order = $this->order->create(
                [
                    "user_id" => Auth()->user()->id,
                    "cart_id" => session('cart_id'),
                    "amount" => $cart->total_price,
                    "charge_id" => $result['data']['id'],
                    "receipt_url" => $result['data']['receipt_url']
                ]
            );

            $this->paymentLog->create(
                [
                    "user_id" => Auth()->user()->id,
                    "cart_id" => session('cart_id'),
                    "status" => "ok",
                    "api_response" => json_encode($result['data']),
                ]
            );

            session()->forget('cart_id');
            return redirect()->route('order.thankyou', ["order" => $order->id]);
        } else {
            $this->paymentLog->create(
                [
                    "user_id" => Auth()->user()->id,
                    "cart_id" => session('cart_id'),
                    "status" => $result['status'],
                    "error_message" => $result['error'],
                    "api_response" => json_encode($result['data']->getRawOutput()),
                ]
            );
            return redirect()->back()->withErrors([$result['error']]);
        }
    }

    public function thankyou(Order $order, OrderExist $request)
    {
        if (Gate::denies('order-id-check', $order)) {
            return redirect()->back();
        }   
        return view('order.thankyou', ["order" => $order]);
    }
}
