<?php

namespace App\Http\Controllers;

use App\Services\CartService;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Requests\UserStore;
use App\Models\User;
use App\Http\Requests\UserEdit;

class AdminUserController extends Controller
{
    public function __construct(UserService $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_user.index', ["users" => $this->user->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserStore $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStore $request)
    {
        $this->user->create($request->all());
        return redirect()->route("users.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin_user.show', ["user" => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin_user.edit', ["user" => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserEdit
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserEdit $request, User $user)
    {
        $this->user->update($user->id, $request->all());
        return redirect()->route('users.show', ['user' => $user->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
