<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\PaymentLog;
use Illuminate\Support\Collection;

class PaymentLogRepository extends BaseRepository
{
    public $sortBy = 'payment_log.id';

    public function __construct(PaymentLog $model)
    {
        $this->model = $model;
    }
}
