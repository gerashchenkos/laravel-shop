<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Cart;
use Illuminate\Support\Collection;

class CartRepository extends BaseRepository
{
    public function __construct(Cart $model)
    {
        $this->model = $model;
    }

    public function show(int $id): Cart
    {
        try {
            $data = $this->model->where("id", $id)->with('cart_product')->first();
        } catch (Throwable $th) {
            //log here
        }
        return $data;
    }
}
