<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\CartProduct;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Repositories\Interfaces\CartProductInterface;
use Illuminate\Support\Facades\DB;

class CartProductRepository extends BaseRepository implements CartProductInterface
{
    use SoftDeletes;

    public function __construct(CartProduct $model)
    {
        $this->model = $model;
    }

    public function addProduct(int $cartId, int $productId, int $quantity = 1)
    {
        $this->model->insertOrIgnore(
            [
                "product_id" => $productId,
                "cart_id" => $cartId,
                "quantity" => $quantity,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]
        );
    }

    public function recalculateTotalPrice(int $cartId): float
    {
        return $this->model->where("cart_id", $cartId)->join(
            'products',
            'cart_products.product_id',
            '=',
            'products.id'
        )->sum(
            \DB::raw('cart_products.quantity * price')
        );
    }

    public function deleteProduct(int $cartId, int $productId): bool
    {
        return $this->model->where("cart_id", $cartId)->where("product_id", $productId)->delete();
    }

    public function updateProduct(int $cartId, int $productId, array $data): bool
    {
        return $this->model->where("cart_id", $cartId)->where("product_id", $productId)->update($data);
    }

    public function getPopular(int $limit): Collection
    {
        return $this->model->select('product_id', DB::raw('count(*) as total'))
            ->groupBy('product_id')->orderBy('total', 'desc')->with('product')->limit($limit)->get();
    }
}
