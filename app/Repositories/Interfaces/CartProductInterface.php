<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface CartProductInterface
{
    public function addProduct(int $cartId, int $productId, int $quantity = 1);

    public function recalculateTotalPrice(int $cartId): float;

    public function deleteProduct(int $cartId, int $productId): bool;

    public function updateProduct(int $cartId, int $productId, array $data): bool;
}
