<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Models\Cart;

interface CartInterface
{
    public function show(int $id): Cart;
}
