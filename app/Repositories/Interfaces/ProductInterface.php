<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface ProductInterface
{
    public function getByCategories(array $categories): Collection;

    public function search(string $word): Collection;
}
