<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Product;
use Illuminate\Support\Collection;
use App\Repositories\Interfaces\ProductInterface;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository implements ProductInterface
{
    public $sortBy = 'products.id';

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function getByCategories(array $categories): Collection
    {
        return $this->model->whereIn("category_id", $categories)->get();
    }

    public function all(int $cartId = null): Collection
    {
        $res = $this->model
            ->leftJoin(
                'cart_products',
                function ($join) use ($cartId) {
                    $join->on('products.id', "=", "cart_products.product_id");
                    if (!empty($cartId)) {
                        $join->where('cart_products.cart_id', "=", (int) $cartId);
                    } else{
                        $join->where('cart_products.cart_id', "=", 0);
                    }
                }
            )
            ->orderBy($this->sortBy, $this->sortOrder)
            ->with('category')
            ->get(['cart_products.cart_id', 'products.*']);
        return $res;
    }

    public function search(string $word): Collection
    {
        return $this->model->where('name', 'like', '%'.$word.'%')->get();
    }

}
