<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Order;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class OrderRepository extends BaseRepository
{
    public $sortBy = 'orders.id';

    public function __construct(Order $model)
    {
        $this->model = $model;
    }
}
