<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Models\Cart;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    public function testCartTest()
    {
        $cart = new Cart();
        $cart->user_id = 1;
        $cart->total_price = 10;
        $this->assertNull($cart->created_at);
    }
}
