<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\User;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class ExampleTest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test404Test()
    {
        $response = $this->get('/aa');

        $response->assertStatus(404);
    }

    /*public function testRegistrationTest()
    {
        $user = [
            'name' => 'Joe',
            'email' => 'testemail@test.com',
            'password' => 'passwordtest',
            'password_confirmation' => 'passwordtest'
        ];

        $response = $this->post('/register', $user);
        $response->assertStatus(302)
            ->assertRedirect('/product');
    }*/

    public function testAuthTest()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get('/');

        $response->assertSeeText("Home", $escaped = true);
    }

    public function testAuthLoginTest()
    {
        $this->withMiddleware();
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get('/');

        $response->assertDontSeeText("Login", $escaped = true);
    }

    public function testAdminUnauthorizedTest()
    {
        $this->withMiddleware();
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $response = $this->get('/admin/test');
        $response->assertUnauthorized();
    }
}
