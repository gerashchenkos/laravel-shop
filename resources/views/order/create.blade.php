@extends('layouts.customer')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4>Confirm Purchase</h4>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{route('order.store')}}">
                        @csrf
                        <div class="form-group" id="card-number-field">
                            <label for="cardNumber">Card Number</label>
                            <input type="text" name="card_number" required class="form-control" id="cardNumber">
                        </div>
                        <div class="form-group" id="expiration-date">
                            <label>Expiration Date</label>

                            <div class="row">
                                <div class="col-md-6">
                                    <select class="form-control" name="exp_month">
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <select class="form-control" name="exp_year">
                                        @for($i = 2020; $i < 2039; $i++)
                                            <option value="{{$i}}"> {{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group CVV">
                            <label for="cvv">CVV</label>
                            <input type="text" name="cvv" class="form-control" required id="cvv">
                        </div>
                        <div class="form-group" id="credit_cards">
                        </div>
                        <div class="form-group" id="pay-now">
                            <button type="submit" class="btn btn-primary" id="confirm-purchase">Confirm</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <h3>Order Summary:</h3>
                    @foreach ($cart->cart_product as $cproduct)
                        <p><b>{{ $cproduct->product->name }}</b> X {{ $cproduct->quantity }} = UAH
                            @money($cproduct->product->price * $cproduct->quantity)</p>
                    @endforeach
                    <hr>
                    <h4>Total: @money($cart->total_price)</h4>
                </div>
            </div>
        </div>
    </div>
@endsection
