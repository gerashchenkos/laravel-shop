@extends('layouts.customer')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-check"></i> Thank you for your purchase!</h5>
                  Your order id #{{$order->id}}. You can see your receipt <a href="{{$order->receipt_url}}" target="_blank">here</a>
                </div>
            </div>
        </div>
    </div>
@endsection
