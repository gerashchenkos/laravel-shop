@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
    <div class="card-body">
        <form method="POST" action="{{route("users.store")}}">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">User Creation</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Name</label>
                                <input type="text" name="name" value="{{ old('name') }}" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                @if($errors->has('name'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">Email</label>
                                <input type="email" name="email" value="{{ old('email') }}" id="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}">
                                @if($errors->has('email'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputStatus">Status</label>
                                <select name="status" class="form-control custom-select {{ $errors->has('status') ? 'is-invalid' : '' }}">
                                    <option selected="" disabled="">Select one</option>
                                    <option {{ old('status') == 'Active' ? "selected" : "" }} value="Active">Active</option>
                                    <option {{ old('status') == 'Locked' ? "selected" : "" }} value="Locked">Locked</option>
                                    <option {{ old('status') == 'Inactive' ? "selected" : "" }} value="Inactive">Inactive</option>
                                </select>
                                @if($errors->has('status'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">Password</label>
                                <input type="password" name="password" id="inputPassword" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}">
                                @if($errors->has('password'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputPasswordConf">Confirm Password</label>
                                <input type="password" name="password_confirmation" id="inputPasswordConf"
                                       class="form-control">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <a href="/admin" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Create new User" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script>
    </script>
@stop
