@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
    <div class="card-body">
        <div id="example1_wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Actions
                            </th>
                            <th>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->status }}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                        <span class="sr-only">Toggle Dropdown</span>
                                        <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-1px, 37px, 0px);">
                                            <a class="dropdown-item" onclick="location.href = '/admin/users/{{ $user->id }}';" href="/admin/users/{{ $user->id }}">Show</a>
                                            <a class="dropdown-item" onclick="location.href = '/admin/users/{{ $user->id }}/edit';" href="/admin/users/{{ $user->id }}/edit">Edit</a>
                                            <!--<a class="dropdown-item" href="#">Delete</a>-->
                                        </div>
                                    </button>
                                </div>
                            </td>
                            <td>
                                <form>
                                    <input type="submit" class="btn btn-danger" value="DELETE">
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@stop
