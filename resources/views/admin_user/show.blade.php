@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6 align-items-stretch">
                <div class="card bg-light">
                    <div class="card-header text-muted border-bottom-0">
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-9">
                                <h2 class="lead"><b>{{ $user->name }}</b></h2>
                                <p class="text-muted text-sm">{{ $user->email }}</p>
                                <p class="text-muted text-sm"><b>Type:</b> {{ $user->type }}</p>
                                <p class="text-muted text-sm"><b>Status:</b> {{ $user->status }}</p>
                                <p class="text-muted text-sm"><b>Incorrect Logins:</b> {{ $user->incorrect_login_attempts }}</p>
                                <p class="text-muted text-sm"><b>Create Time:</b> {{ $user->created_at }}</p>
                            </div>
                            <div class="col-5 text-center">
                                <img src="../../dist/img/user1-128x128.jpg" alt="" class="img-circle img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>

    </script>
@stop
