@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="/photos" method="POST">
        @csrf
        <div class="form-group">
            <label for="email">Photo name:</label>
            <input type="text" name="name" value="{{ old('name') ?? $name }}" class="form-control @error('name') is-invalid @enderror" id="email">
        </div>
        <div class="form-group">
            <label for="pwd">Photo description:</label>
            <input type="text" name="description" value="{{ old('description') }}" class="form-control @error('description') is-invalid @enderror" id="pwd">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
