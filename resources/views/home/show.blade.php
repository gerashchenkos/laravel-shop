@extends('layouts.app')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h2>Hello {{$name}}</h2>
@endsection
