@foreach ($products as $product)
    <div class="col-md-4">
        <div class="card mb-4 shadow-sm">
            @if (!empty($product->image))
                <img class="img-fluid product-img"
                     src="/storage/products/{{ $product->image }}">
            @else
                <img class="img-fluid product-img"
                     src="/storage/products/{{ config('variables.default_product_image') }}">
            @endif
            <div class="card-body">
                <p class="card-text">{{ $product->name }}</p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group chechbox-block">
                        <label class="form-check-label" for="exampleCheck1">Check me</label>
                        <input
                            class="form-check-input" type="checkbox"
                            @if($product->cart_id) checked @endif name="products[]"
                            value="{{ $product->id }}">
                    </div>
                    <small class="text-muted">UAH @money($product->price)</small>
                </div>
            </div>
        </div>
    </div>
@endforeach
