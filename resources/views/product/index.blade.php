@extends('layouts.customer')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row left-side-bar">
                <div class="col-md-2">
                    <input type="text" name="birthday" value="10/24/1984"/>
                    <form method="POST" action="{{ route('product.filtered') }}">
                        @csrf
                        <h5>Categories:</h5>
                        @foreach ($categories as $category)
                            <input class="form-check-input" type="checkbox" name="categories[]"
                                   value="{{ $category->id }}"
                                   @if (!empty($selected_categories) && in_array(
                                        $category->id,
                                        $selected_categories
                                    )) checked
                                @endif />
                            {{ $category->name }} <br>
                        @endforeach
                        <input type="submit" class="btn btn-secondary btn-sm" value="Apply"/>
                    </form>
                </div>
            </div>
            <form method="POST" action="{{ route('cart.store') }}">
                @csrf
                <div class="row justify-content-end">
                    <input type="text" id="search" class="" placeholder="Search" value=""/>
                    <button type="button" onclick="searchProducts()"><i class="fa fa-search"></i></button>
                </div>
                <div class="row justify-content-center">
                    <h3>Top Products</h3>
                </div>
                <div class="row">
                    @foreach ($popularProducts as $product)
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                @if (!empty($product['product']['image']))
                                    <img class="img-fluid product-img"
                                         src="/storage/products/{{ $product['product']['image'] }}">
                                @else
                                    <img class="img-fluid product-img"
                                         src="/storage/products/{{ config('variables.default_product_image') }}">
                                @endif
                                <div class="card-body">
                                    <p class="card-text">{{ $product['product']['name'] }}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group chechbox-block">
                                        </div>
                                        <small class="text-muted">UAH @money($product['product']['price'])</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="col-md-12 mb-4">
                        <hr>
                    </div>

                    <div id="products-block" class="row col-md-12 mb-4">
                        @include('product.products_block')
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 center-block">
                    </div>
                    <div class="col-md-4 center-block">
                        <input type="submit" class="btn btn-primary order-button" value="Order"/>
                    </div>
                    <div class="col-md-4 center-block">
                    </div>
                </div>
            </form>

            <a href="/order/thankyou/1">Test</a>
            @error('id')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
@endsection

@section('page-js-files')
    <!--<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>-->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@stop

@section('page-js-script')
    <script type="text/javascript">
        async function searchProducts () {
            const word = document.getElementById('search').value
            const csrfToken = document.head.querySelector('[name~=csrf-token][content]').content
            if (word) {
                let product = {
                    name: word,
                }
                let response = await fetch('/product/search', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8',
                        'X-CSRF-Token': csrfToken,
                    },
                    body: JSON.stringify(product),
                })

                let result = await response.json()
                if (result.message == 'OK') {
                    document.getElementById('products-block').innerHTML = result.products
                }
            }
        }
    </script>
@stop
