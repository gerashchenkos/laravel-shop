@extends('layouts.customer')

@section('content')
    <div class="album py-5 bg-light">
        <div class="container">
            <form method="POST" action="{{route('cart.update')}}">
                @csrf
                <div class="row">
                    @foreach ($cart->cart_product as $cproduct)
                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                @if (!empty($cproduct->product->image))
                                    <img class="img-fluid product-img"
                                         src="/storage/products/{{ $cproduct->product->image }}">
                                @else
                                    <img class="img-fluid product-img"
                                         src="/storage/products/{{ config('variables.default_product_image') }}">
                                @endif
                                <div class="card-body">
                                    <p class="card-text">{{ $cproduct->product->name }}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="form-group chechbox-block">
                                            <label class="" for="exampleCheck1">Quantity</label>
                                            <input class="form-control" type="number"
                                                   value="{{ $cproduct->quantity }}" id="quantity"
                                                   name="quantity_{{ $cproduct->product_id }}" min="0"
                                                   max="{{ $cproduct->product->quantity }}">
                                        </div>
                                        <small class="text-muted">UAH @money($cproduct->product->price)</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-2 center-block">
                    </div>
                    <div class="col-md-8 center-block">
                        <input type="submit" name="update" class="btn btn-primary order-button" value="UPDATE"/>
                        <a name="reset" class="btn btn-primary order-button" href="{{ route('cart.reset') }}">RESET</a>
                        <a name="reset" class="btn btn-primary order-button" href="{{ route('order.create') }}">ORDER</a>
                    </div>
                    <div class="col-md-2 center-block">
                    </div>
                </div>
            </form>
        </div>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-2">
                    <h2>Total Price:</h2>
                </div>
                <div class="col-md-4">
                    <h2>$ @money($cart->total_price)</h2>
                </div>
            </div>
        </div>
    </div>
@endsection
