@extends('adminlte::page')

@section('title', 'Products')

@section('content')
    <div class="card-body">
        <div id="example1_wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Price
                            </th>
                            <th>
                                Category Id
                            </th>
                            <th>
                                Actions
                            </th>
                            <th>

                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "ajax": '{!! route('admin.products_data') !!}',
                "columns": [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'price', name: 'price' },
                    { data: 'category_name', name: 'category' },
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>
@stop
