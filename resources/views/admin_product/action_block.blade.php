<div class="btn-group">
    <button type="button" class="btn btn-info">Action</button>
    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
        <span class="sr-only">Toggle Dropdown</span>
        <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-1px, 37px, 0px);">
            <a class="dropdown-item" onclick="location.href = '/admin/products/{{ $product['id'] }}';" href="/admin/products/{{ $product['id'] }}">Show</a>
            <a class="dropdown-item" onclick="location.href = '/admin/products/{{ $product['id'] }}/edit';" href="/admin/products/{{ $product['id'] }}/edit">Edit</a>
            <a class="dropdown-item" onclick="location.href = '/admin/export/product/{{ $product['id'] }}';" href="/admin/export/product/{{ $product['id'] }}">Export PDF</a>
        </div>
    </button>
</div>
