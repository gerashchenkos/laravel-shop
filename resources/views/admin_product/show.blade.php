@extends('adminlte::page')

@section('title', 'Show Product')

@section('content')
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6 align-items-stretch">
                <div class="card bg-light">
                    <div class="card-header text-muted border-bottom-0">
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-9">
                                <h2 class="lead"><b>#{{ $product->id }} {{ $product->name }}</b></h2>
                                <p class="text-muted text-sm">$@money($product->price)</p>
                                <p class="text-muted text-sm"><b>Quantity:</b> {{ $product->quantity }}</p>
                                <p class="text-muted text-sm"><b>Category:</b> #{{ $product->category_id }} {{ $product->category->name }}</p>
                                <p class="text-muted text-sm">
                                    @if (!empty($product->image))
                                        <img class="img-fluid product-img" src="/storage/products/{{ $product->image }}">
                                    @else
                                        <img class="img-fluid product-img"
                                             src="/storage/products/{{ config('variables.default_product_image') }}">
                                    @endif
                                </p>
                                <p class="text-muted text-sm"><b>Create Time:</b> {{ $product->created_at }}</p>
                            </div>
                            <div class="col-5 text-center">
                                <img src="../../dist/img/user1-128x128.jpg" alt="" class="img-circle img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>

    </script>
@stop
