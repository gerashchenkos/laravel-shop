@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
    <div class="card-body">
        <form method="POST" action="{{route("products.update", ['product' => $product->id])}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Product Edit</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Name</label>
                                <input type="text" name="name" value="{{ old('name') ?? $product->name }}" id="name"
                                       class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                @if($errors->has('name'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">Price</label>
                                <input type="text" name="price" value="{{ old('price') ?? $product->price }}" id="price"
                                       class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}">
                                @if($errors->has('price'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputStatus">Quantity</label>
                                <input type="text" name="quantity" value="{{ old('quantity') ?? $product->quantity }}" id="quantity"
                                       class="form-control {{ $errors->has('quantity') ? 'is-invalid' : '' }}">
                                @if($errors->has('quantity'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="inputStatus">Category</label>
                                <select name="category_id"
                                        class="form-control custom-select {{ $errors->has('category_id') ? 'is-invalid' : '' }}">
                                    <option selected="" disabled="">Select category</option>
                                    @foreach($categories as $category)
                                        <option
                                            {{ (old('category_id') == $category->id || (empty(old('category_id')) && $product->category_id == $category->id)) ? "selected" : "" }} value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('category_id'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                @if (!empty($product->image))
                                    <img class="img-fluid product-img" src="/storage/products/{{ $product->image }}">
                                @endif
                                <label for="exampleInputFile">File input</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="image"
                                               class="custom-file-input {{ $errors->has('image') ? 'is-invalid' : '' }}"
                                               id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose new file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Change</span>
                                    </div>
                                </div>
                                @if($errors->has('image'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <a href="/admin" class="btn btn-secondary">Cancel</a>
                    <input type="submit" value="Edit Product" class="btn btn-success float-right">
                </div>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script>
    </script>
@stop
